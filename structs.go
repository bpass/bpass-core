package main

import (
	"crypto/sha256"
	"hash"
)

type Signature struct {
	Hash      hash.Hash
	Signature []byte
}

type Block struct {
	PrevHash Signature
	Hash     Signature
	Data     []byte
}

func NewBlock(prevHash hash.Hash, data []byte) Block {
	return Block{
		PrevHash: Signature{Hash: prevHash, Signature: nil},
		Hash:     Signature{Hash: sha256.New(), Signature: nil},
		Data:     data,
	}
}

func (*Block) Validate() bool {
	return true
}
