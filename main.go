package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
	log "github.com/sirupsen/logrus"
	"os"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/storage/memory"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"strings"
	"github.com/spf13/viper"
	"strconv"
	"time"
	"gopkg.in/src-d/go-billy.v4/osfs"
)

var (
	app        = kingpin.New("bpass", "Store passwords in the block chain - simple and secure")
	verbose    = app.Flag("verbose", "Enable verbose output").Short('v').Bool()
	add        = app.Command("add", "Add a password to the block chain")
	force      = add.Flag("force", "Force push to the block chain repository").Bool()
	password   = add.Flag("password", "Specify the password to push").Required().String()
	validate   = app.Command("validate", "Validate the block chain repository")
	repository string
)

func getRepository() *git.Repository {
	// Clone repository in-memory
	fs := osfs.New("/tmp/blockchain/")
	r, err := git.Clone(memory.NewStorage(), fs, &git.CloneOptions{
		URL: repository,
	})
	if err != nil {
		log.Fatal(err)
	}
	return r
}

func ValidateCommit(commit *object.Commit) {
	// Check if commit is a valid commit
	// TODO: Check automatically if first commit
	// Check if parent element has the valid crypt hash
	if commit.ParentHashes[0].String() != strings.Split(commit.Message, "\n")[0] {
		log.Fatal("The block chain is invalid! This error occured at hash: ", commit.Hash)
	}
	log.Debug("Checked hash ", commit.Hash.String())
}

func AddPassword() {
	// Add a password to the blockchain
	r := getRepository()
	worktree, err := r.Worktree()
	if err != nil {
		log.Fatal(err)
	}
	file, err := worktree.Filesystem.OpenFile("Countfile", os.O_WRONLY|os.O_APPEND, os.ModeAppend)
	if err != nil {
		log.Fatal(err)
	}
	head, err := r.Head()
	if err != nil {
		log.Fatal(err)
	}
	log.Debug("Getting head commit hash")
	commit, err := r.CommitObject(head.Hash())
	if err != nil {
		log.Fatal(err)
	}
	log.Debug("Updating Countfile")
	_, err = file.Write([]byte(strconv.Itoa(commit.NumParents())))
	if err != nil {
		log.Fatal(err)
	}
	file.Close()
	hashes, err := worktree.Add("Countfile")
	if err != nil {
		log.Fatal(err)
	}
	worktree.Commit(commit.Hash.String()+"\n"+*password, &git.CommitOptions{
		Author: &object.Signature{Name: "bpass", Email: "bpass@localhost", When: time.Now()},
	})
	log.Debug(hashes.String())
	r.Push(&git.PushOptions{})
	log.Info("Added password to the blockchain")
}

func validateRepository() {
	r := getRepository()
	commits, err := r.CommitObjects()
	if err != nil {
		log.Fatal(err)
	}
	log.Info("Verifying integrity of block chain!")
	commits.ForEach(func(commit *object.Commit) error {
		log.Debug("Validating commit ", commit.Hash)
		if commit.NumParents() == 0 {
			// If first commit, ignore
			return nil
		}
		ValidateCommit(commit)
		return nil
	})
	log.Info("The block chain is valid!")
}

func main() {
	app.Parse(os.Args[1:])

	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.SetConfigType("yaml")
	if viper.ReadInConfig() != nil {
		log.Fatal("Failed to read config")
	}

	if viper.GetString("repository") == "" {
		log.Fatal("Please set the repository field in the configuration file")
	}
	repository = viper.GetString("repository")

	log.SetLevel(log.InfoLevel)
	if *verbose {
		log.SetLevel(log.DebugLevel)
	}

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	// Register pw
	case add.FullCommand():
		AddPassword()
	case validate.FullCommand():
		validateRepository()
	}

}
